<?php
/**
 * @file
 * Callbacks for the jZebra module
 *
 * Requires jZebra (http://code.google.com/p/jzebra/).
 */

/**
 * Displays the shipping label for printing.
 *
 * Each argument is a component of the file path to the image.
 *
 * @ingroup themeable
 */
function jzebra_print_file_dialog() {
  global $base_path;
  $args = func_get_args();
  $file_path = 'public://' . implode('/', $args);
  $file_url = file_create_url($file_path);
  
  if (!file_exists($file_path)) {
    return drupal_not_found();
  }
  if (substr($file_url, -4) != '.zpl') {
    return drupal_access_denied();
  }
  
  return _jzebra_print_dialog_output($file_url);
}

/**
 * Displays the shipping label from a url (view) for printing.
 *
 * Each argument is a component of the file path to the image.
 *
 * @ingroup themeable
 */
function jzebra_print_dialog() {
  $args = func_get_args();
  
  // The rest of the url becomes the url of view 
  $url = url(implode('/', $args));
  
  // TODO: Maybe add check that url is valid/exists.

  return _jzebra_print_dialog_output($url);
}


function _jzebra_print_dialog_output($source_url) {
    
  // Settings for JS
  $library_path = libraries_get_path('jzebra');
  $settings = array(
    'file_path' => $source_url,
    'library_path' => url($library_path),
  );
  
  drupal_add_js(array('jzebra' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'jzebra') . '/jzebra_applet_controller.js');

  $output = '<p id="jzebra_message_loading">' . t('jZebra is loading, please wait... If prompted, you may need to click "Allow Blocked Content" or "Always Run On This Site."') . '</p>';
  $output .= '<div id="jzebra_applet_container"></div>';
  $output .= '<p id="jzebra_message_version" style="display: none">' . t('jZebra version !version initialized successfully.', array('!version' => '<span id="jzebra_version"></span>')) . '</p>';
  $output .= '<div id="jzebra_printers" style="display: none"><p>' . t('Available Printers:') . '</p><ul></ul></div>';
  $output .= '<div id="printnow" style="display: none;"><form method="get" action=""><input type="submit" name="submit" value="' . t('Print Now') . '" /></form></div>';

  return $output;
}
